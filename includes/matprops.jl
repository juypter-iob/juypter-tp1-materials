module matprops
# Example usage
# air_dens_tuple = readProperties("AIR.csv" , "rho") 
# air_dens_T_arr = [getProperty(air_dens_tuple, i) for i in collect(Float64,0:25:3050)]
# dens_at_T = getProperty(air_dens_tuple, 355)

using DelimitedFiles
using Unitful
using Unitful.DefaultSymbols

export read_properties, interp_property, interp_property_bounded

function read_properties(fn::String , prop::String) 

    anyvec = readdlm(fn, ';')
    names = convert(Vector{String},anyvec[1,:])
    unit_multiplicators = convert(Vector{Float64}, anyvec[2,:])
    unit_strs = convert(Vector{String},anyvec[3,:])
    data_raw = view(anyvec,4:size(anyvec,1),:)
    bdata_empty = (data_raw .== "")
    data_raw[bdata_empty] .= nothing
    data = convert(Matrix{Union{Float64,Nothing}},data_raw)

    colno = findfirst( f -> f .== prop , names)

    if !isnothing(colno)
        
        data_view = view(data, :, colno)
        prop_data = convert(Array{Float64}, data[data_view .!= nothing, colno])
        T = convert(Array{Float64}, data[data_view .!= nothing,1])
        unit_str = unit_strs[colno]
        unit_multiplicator = unit_multiplicators[colno]
        
        if (!isempty(prop_data) || !isempty(T))
            return (unit_str, unit_multiplicator, T, prop_data)
        else
            error("no values in of property " * prop * " in file "* fn)
            return nothing
        end
    else
        error("no property " * prop * " in file "* fn)
        return nothing
    end
end


function interp1_bounded(xarr,yarr, v)
    #xarr musst be sorted low to hight
    tol = 1E-8
    
    if v <= xarr[1]
        return yarr[1]
    end
    if v >= xarr[end]
        return yarr[end]
    end

    xlow = findfirst(f -> f .> (v+tol), xarr)

    res = ((yarr[xlow] - yarr[xlow-1])/(xarr[xlow] - xarr[xlow-1]) 
        * (v-xarr[xlow-1]) + yarr[xlow-1])

    return res
end


function interp1_extra(xarr,yarr, v)
    #xarr musst be sorted low to hight
    tol = 1E-8
    
    if v <= xarr[1]
        xlow = 2
        res = ((yarr[xlow] - yarr[xlow-1])/(xarr[xlow] - xarr[xlow-1]) 
        * (v-xarr[xlow-1]) + yarr[xlow-1])
        return res
    end
    if v >= xarr[end]
        xlow = length(xarr)
        res = ((yarr[xlow] - yarr[xlow-1])/(xarr[xlow] - xarr[xlow-1]) 
        * (v-xarr[xlow-1]) + yarr[xlow-1])
        
        return res
    end

    xlow = findfirst(f -> f .> (v+tol), xarr)

    res = ((yarr[xlow] - yarr[xlow-1])/(xarr[xlow] - xarr[xlow-1]) 
        * (v-xarr[xlow-1]) + yarr[xlow-1])

    return res
end



# function getProperty(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, 
#         T) 
    
#     return "to implement ..."
# end





function interp_property_bounded(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, T::Float64)
    # TODO make UNIT aware if T uses unitful units
    # also return prop with units
    
    unit = prop_data[1]
    weight = prop_data[2]
    Tarr = prop_data[3]
    prop = prop_data[4]

    return interp1_bounded(Tarr, prop, T)
end


function interp_property(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, T::Float64)
    # TODO make UNIT aware if T uses unitful units
    # also return prop with units
    
    unit = prop_data[1]
    weight = prop_data[2]
    Tarr = prop_data[3]
    prop = prop_data[4]

    return interp1_extra(Tarr, prop, T)
end

function interp_property(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, T::Vector{Float64})
    return  [interp_property(prop_data,Ti) for Ti in T]
end

function interp_property_bounded(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, T::Vector{Float64})
    return  [interp_property_bounded(prop_data,Ti) for Ti in T]
end


function interp_property(prop_data::Tuple{String,Float64,Array{Float64,1},Array{Float64,1}}, 
        Temp::T) where T <: Unitful.Quantity
    
    unit = prop_data[1]
    weight = prop_data[2]
    Tarr = prop_data[3]
    prop = prop_data[4]
    
    return "to implement unitful ..."
end


end